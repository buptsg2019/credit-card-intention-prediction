import numpy as np
import pandas as pd

# ans = np.loadtxt('ans.csv', delimiter=',')
# print(ans.shape)
#
# post_ans = np.zeros((49145,), dtype='Float64')
# for i in range(49145):
#     x = ans[i]
#     if x > 0.8:
#         x += 0.05
#         if x > 1.:
#             x = 1.
#     elif x < 0.2:
#         x -= 0.05
#         if x < 0.:
#             x = 0.
#     post_ans[i] = x
# np.savetxt('./post_ans.csv', post_ans, fmt='%.8f', delimiter=',')

xgb_ans = np.loadtxt('xgboost_ans.csv', delimiter=',')
cat_ans = np.loadtxt('catboost_ans.csv', delimiter=',')

ans = np.stack((xgb_ans, cat_ans), axis=1)
print(ans.shape)

diff = 0
threshold = 0.01
cnt = 0

post_ans = np.zeros((49145,), dtype='Float64')
for i in range(49145):
    x = ans[i]
    y = np.around(x)
    post_ans[i] = (x[0] + x[1]) / 2
    if abs(x[0] - x[1]) > threshold:
        diff += 1
        if y[0] != y[1]:
            print(x)
            cnt += 1
    elif y[0] == y[1]:
        post_ans[i] = y[0]

print('diff', diff)
print('cnt', cnt)
np.savetxt('./post_ans.csv', post_ans, fmt='%.8f', delimiter=',')

